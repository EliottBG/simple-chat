const express = require('express');
const app = express();
const http = require('http').Server(app);
const path = require('path');
const favicon = require('serve-favicon');
const socketio = require('socket.io')(http);

app.use(express.static(path.join(dirname, 'static')));
app.use(favicon(path.join(dirname, 'static', 'images', 'favicon.ico')));

http.listen(process.env.PORT || 5000, () => {
  console.log(Server running at 127.0.0.1:${process.env.port || 5000});
});

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, 'index.html'));
});

socketio.on('connection', (socket) => {
  console.log('Socket connected');

  socket.on('message', (msg) => {
    console.log('Received message:', msg);

// **Validation du message reçu**
if (!msg || typeof msg !== 'string') {
  return;
}

// **Echappement des caractères spéciaux**
const escapedMsg = msg
  .replace(/&/g, '&amp;')
  .replace(/</g, '&lt;')
  .replace(/>/g, '&gt;')
  .replace(/"/g, '&quot;')
  .replace(/'/g, '&apos;');

if (escapedMsg === '/date') {
  const currentDate = new Date().toLocaleDateString();
  socketio.emit('message', currentDate);
} else {
  socketio.emit('message', escapedMsg);
}
  });

  socket.on('disconnect', () => {
    console.log('Socket disconnected');
  });
});